from cgitb import handler
from flask import Flask, request, abort
from linebot import LineBotApi, WebhookHandler
from linebot.exceptions import InvalidSignatureError
from linebot.models import MessageEvent, TextMessage, TextSendMessage
import configparser
import crawer

app = Flask(__name__)
# linebot的基本資料
config = configparser.ConfigParser()
config.read('config.ini')
lint_bot_api = LineBotApi(config.get('line_bot', 'Channel_Access_Token'))
handler = WebhookHandler(config.get('line_bot', 'Channel_Secret'))

# 接line的資訊
@app.route('/callback', methods=['POST'])
def callback():
    signature = request.headers['X-Line-Signature']# X-Line-Signature: 數位簽章
    body = request.get_data(as_text=True)
    app.logger.info('Request body: ' + body)

    try:
        print(body, signature)
        handler.handle(body, signature)

    except InvalidSignatureError:
        abort(400)
    return 'OK'

# 可以針對收到的訊息種類作出處理，這邊是針對 TextMessage
@handler.add(MessageEvent, message=TextMessage)
def echo(event):
    if event.message.text == "省錢":  # 如果line輸入省錢
        temp_title = []

        # title+href
        results = crawer.pttCrawer("Lifeismoney") #爬省錢版
        for result in results:
            temp_title.append(
                result['title']+' '+result['url'])

        print(temp_title[0]+'\n\n'+temp_title[1]+'\n\n' +
              temp_title[2]+'\n\n'+temp_title[3]+'\n\n'+temp_title[4])
        # 將要發出去的文字變成TextSendMessage
        lint_bot_api.reply_message(event.reply_token, TextSendMessage(text=temp_title[0]+'\n\n'+temp_title[1]+'\n\n' +
                                                                      temp_title[2]+'\n\n'+temp_title[3]+'\n\n'+temp_title[4]))


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=8000)
